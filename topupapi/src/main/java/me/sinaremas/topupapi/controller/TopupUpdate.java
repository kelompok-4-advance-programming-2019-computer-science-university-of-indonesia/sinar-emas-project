package me.sinaremas.topupapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import me.sinaremas.topupapi.util.TopupOwnership;
import me.sinaremas.topupapi.dao.TopupRepository;

@RestController()
public class TopupUpdate {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    TopupRepository topupRepository;

    @PostMapping(path = "/update/on_buy")
    public ResponseEntity<String> testing(
            @CookieValue("session") String session,
            @ModelAttribute TopupOwnership topupOwnership){

        long userId = topupOwnership.getUserId();
        long topupId = topupOwnership.getTopupId();
        HttpCookie cookie = ResponseCookie.from("session", session)
                .path("/")
                .build();

        try {
            topupRepository.updateOwner(topupId, userId);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body("UPDATE SUCCESS");
        }
        catch(Exception e) {
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body("UPDATE FAILED");
        }
    }
}

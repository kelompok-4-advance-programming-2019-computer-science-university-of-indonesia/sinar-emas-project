package me.sinaremas.topupapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import me.sinaremas.topupapi.model.Topup;


@Transactional
public interface TopupRepository extends JpaRepository<Topup,Long> {
    @Query("SELECT COUNT(*) FROM Topup")
    Long getNumberOfItem();

    @Modifying
    @Query("UPDATE Topup i SET i.ownerId = ?2 WHERE i.id = ?1")
    void updateOwner(long topupId, long ownerId);
}
package me.sinaremas.topupapi.util;

public class TopupOwnership {
    private long userId;
    private long topupId;

    public long getUserId() {
        return this.userId;
    }

    public long getTopupId() {
        return this.topupId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setTopupId(int topupId) {
        this.topupId = topupId;
    }
}

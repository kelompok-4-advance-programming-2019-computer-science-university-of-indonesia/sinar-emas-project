package me.sinaremas.topupapi.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@RestController()
public class UserAuthentication {

    @Autowired
    private RestTemplate restTemplate;


    @RequestMapping(path = "/user-authentication/testing", method = RequestMethod.GET)
    public String testing(){
        String response = restTemplate.getForObject("http://userapi/authenticate/testing/", String.class);
        return response;
    }
}
package me.sinaremas.topupapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TopupApiApplicationTests {		//Fix Class Name

	@Test
	public void serviceAvaibilityTest() {
		String[] args = new String[0];
		TopupApiApplication.main(args);
	}

}

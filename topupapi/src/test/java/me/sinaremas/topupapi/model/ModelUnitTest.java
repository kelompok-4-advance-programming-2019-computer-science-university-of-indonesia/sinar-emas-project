package me.sinaremas.topupapi.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelUnitTest {

    @Test
    public void testAddTopup() {
        Topup topup1 = new Topup();
        topup1.setPrice(20000);
        topup1.setStatus("AVAILABLE");
        topup1.setName("voucher 10000");
        topup1.setOwnerId(1);
        
        assertEquals("error initializing name attribute", "voucher 10000", topup1.getName());
        assertEquals("error initializing status attribute", "AVAILABLE", topup1.getStatus());
        assertEquals("error initializing ownerId attribute", 1, topup1.getOwnerId());
        assertEquals("error initializing price attribute", 20000, topup1.getPrice());

        topup1.setId(10);
        topup1.setName("voucher 50000");
        topup1.setStatus("AVAILABLE");
        topup1.setOwnerId(1);
        topup1.setPrice(55000);

        assertEquals(
                "Setter failed to operate",
                "" + topup1.getId() + " | " + topup1.getName() + " | " + 
                topup1.getStatus() + " | " + topup1.getOwnerId(),
                topup1.toString());
    }

}

package me.sinaremas.topupapi.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import me.sinaremas.topupapi.model.Topup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaoUnitTest {
    @Autowired
    TopupRepository topupRepository;

    @Test
    @Transactional
    @Rollback(false)
    public void testAddTopup() {
        Topup newTestTopup = new Topup();
        newTestTopup.setName("voucher 10000");
        newTestTopup.setOwnerId(1);
        newTestTopup.setPrice(20000);
        long initialCount = topupRepository.getNumberOfItem();
        Topup savedTopup = topupRepository.save(newTestTopup);
        long presentCount = topupRepository.getNumberOfItem();
        assertEquals("Number of data after insertion must increase by one", presentCount, initialCount + 1);
        topupRepository.deleteById(savedTopup.getId());
    }

}

package me.sinaremas.itemapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import me.sinaremas.itemapi.util.ItemOwnership;
import me.sinaremas.itemapi.dao.ItemRepository;

@RestController()
public class ItemUpdate {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ItemRepository itemRepository;

    @PostMapping(path = "/update/on_buy")
    public ResponseEntity<String> updateItem(
            @CookieValue("session") String session,
            @ModelAttribute ItemOwnership itemOwnership){

        try {
            long userId = itemOwnership.getUserId();
            long itemId = itemOwnership.getItemId();
            HttpCookie cookie = ResponseCookie.from("session", session)
                .path("/")
                .build();
            itemRepository.updateOwner(itemId, userId);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body("UPDATE SUCCESS");
        }
        catch(Exception e) {
            return ResponseEntity
                    .ok()
                    .body("UPDATE FAILED");
        }
    }
}

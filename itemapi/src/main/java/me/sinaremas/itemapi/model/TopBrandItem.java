package me.sinaremas.itemapi.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 * The class is Model to Item Databases in MySQL
 * status is an enum {AVAILABLE, ON_CART, ON_DEMAND, SUSPENDED, PURCHASED, ON_DELIVERY, DELIVERED}
 * Author: Febriananda Wida Pramudita
 */

@Entity
@Table(name = "top_brand_item")
public class TopBrandItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @Column(name="name")
    private String name;

    @Column(name="photo_url")
    private String photoUrl;

    @Column(name="description")
    private String description;

    @Column(name="status")
    private String status;

    @Column(name="owner_id")
    private long ownerId;

    @Column(name="price")
    private long price;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getPhotoUrl() {
        return photoUrl;
    }
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public long getOwnerId() {
        return ownerId;
    }
    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public String toString(){
        return "" + id + " | " + name + " | " + photoUrl + " | " + description + " | " + status + " | " + ownerId;
    }

}

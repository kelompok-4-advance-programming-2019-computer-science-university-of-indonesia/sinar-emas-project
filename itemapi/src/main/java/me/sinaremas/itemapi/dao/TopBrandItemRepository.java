package me.sinaremas.itemapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import me.sinaremas.itemapi.model.TopBrandItem;


@Transactional
public interface TopBrandItemRepository extends JpaRepository<TopBrandItem,Long> {
    @Query("SELECT COUNT(*) FROM TopBrandItem ")
    Long getNumberOfItem();
}
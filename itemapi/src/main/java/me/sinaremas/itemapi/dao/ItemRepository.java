package me.sinaremas.itemapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import me.sinaremas.itemapi.model.Item;


@Transactional
public interface ItemRepository extends JpaRepository<Item,Long> {
    @Query("SELECT COUNT(*) FROM Item")
    Long getNumberOfItem();

    @Modifying
    @Query("UPDATE Item i SET i.ownerId = ?2 WHERE i.id = ?1")
    void updateOwner(long itemId, long ownerId);
}
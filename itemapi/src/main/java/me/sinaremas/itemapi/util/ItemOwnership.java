package me.sinaremas.itemapi.util;

public class ItemOwnership {
    private long userId;
    private long itemId;

    public long getUserId() {
        return this.userId;
    }

    public long getItemId() {
        return this.itemId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}

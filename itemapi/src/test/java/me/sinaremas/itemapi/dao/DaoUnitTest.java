package me.sinaremas.itemapi.dao;

import me.sinaremas.itemapi.model.TopBrandItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import me.sinaremas.itemapi.model.Item;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaoUnitTest {
    @Autowired
    ItemRepository itemRepository;

    @Autowired
    TopBrandItemRepository topBrandItemRepository;

    @Test
    @Transactional
    @Rollback(false)
    public void testAddItem() {
        Item newTestItem = new Item();
        newTestItem.setId(10000);
        newTestItem.setName("batarag");
        newTestItem.setPhotoUrl("https://images.unsplash.com/photo-1497353865955-300799f7ef7a");
        newTestItem.setDescription("throw your opponent without kill them");
        newTestItem.setOwnerId(1);
        newTestItem.setPrice(300);
        long initialCount = itemRepository.getNumberOfItem();
        Item savedItem = itemRepository.save(newTestItem);
        long presentCount = itemRepository.getNumberOfItem();
        assertEquals("Number of data after insertion must increase by one", presentCount, initialCount + 1);
        itemRepository.deleteById(savedItem.getId());
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testAddTopBrandItem() {
        TopBrandItem newTestItem = new TopBrandItem();
        newTestItem.setId(10000);
        newTestItem.setName("batarag");
        newTestItem.setPhotoUrl("https://images.unsplash.com/photo-1497353865955-300799f7ef7a");
        newTestItem.setDescription("throw your opponent without kill them");
        newTestItem.setOwnerId(1);
        newTestItem.setPrice(300);
        long initialCount = topBrandItemRepository.getNumberOfItem();
        TopBrandItem savedItem = topBrandItemRepository.save(newTestItem);
        long presentCount = topBrandItemRepository.getNumberOfItem();
        assertEquals("Number of data after insertion must increase by one", initialCount + 1, presentCount);
        topBrandItemRepository.deleteById(savedItem.getId());
    }

}

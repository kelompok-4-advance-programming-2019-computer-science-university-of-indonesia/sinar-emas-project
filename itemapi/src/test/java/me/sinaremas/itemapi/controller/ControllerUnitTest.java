package me.sinaremas.itemapi.controller;

import me.sinaremas.itemapi.dao.ItemRepository;
import me.sinaremas.itemapi.model.Item;
import me.sinaremas.itemapi.util.ItemOwnership;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllerUnitTest {

    @Autowired
    ItemRepository itemRepository;

    @Test
    @Transactional
    @Rollback(false)
    public void testSuccessUpdateItem() throws Exception {
        ItemOwnership itemOwnership = new ItemOwnership();
        itemOwnership.setItemId(1);
        itemOwnership.setUserId(1);

        ItemUpdate itemUpdate = new ItemUpdate();
        itemUpdate.itemRepository = this.itemRepository;
        ResponseEntity<String> responseEntity = itemUpdate.updateItem("", itemOwnership);
        assertEquals("It should be SUCCESS", "UPDATE SUCCESS", responseEntity.getBody());
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testFailedUpdateItem() throws Exception {
        ItemOwnership itemOwnership = new ItemOwnership();
        itemOwnership.setItemId(-1);
        itemOwnership.setUserId(-1);

        ItemUpdate itemUpdate = new ItemUpdate();
        itemUpdate.itemRepository = this.itemRepository;
        ResponseEntity<String> responseEntity = itemUpdate.updateItem(null, null);
        assertEquals("It should be FAILED", "UPDATE FAILED", responseEntity.getBody());
    }
}
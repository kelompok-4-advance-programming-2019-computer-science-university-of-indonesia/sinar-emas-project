package me.sinaremas.itemapi.util;

import java.lang.reflect.Field;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

public class ItemOwnershipUnitTest {

    @Test
    public void testSetter_setsProperly() throws NoSuchFieldException, IllegalAccessException {
        final ItemOwnership owns = new ItemOwnership();
        owns.setUserId(10);
        owns.setItemId(100);
        final Field fieldUserId = owns.getClass().getDeclaredField("userId");
        final Field fieldItemId = owns.getClass().getDeclaredField("itemId");
        fieldUserId.setAccessible(true);
        fieldItemId.setAccessible(true);
        assertEquals("User field didn't match", fieldUserId.get(owns), 10L);
        assertEquals("itemid field didn't match", fieldItemId.get(owns), 100L);
    }

    @Test
    public void testGetter_getsValue() throws NoSuchFieldException, IllegalAccessException {
        final ItemOwnership owns = new ItemOwnership();
        final Field fieldUserId = owns.getClass().getDeclaredField("userId");
        final Field fieldItemId = owns.getClass().getDeclaredField("itemId");
        fieldUserId.setAccessible(true);
        fieldItemId.setAccessible(true);
        fieldUserId.set(owns, 10);
        fieldItemId.set(owns, 100);
        final long resultUserId = owns.getUserId();
        final long resultItemId = owns.getItemId();
        assertEquals("field userid wasn't retrieved properly", resultUserId, 10);
        assertEquals("field itemid wasn't retrieved properly", resultItemId, 100);
    }

}
package me.sinaremas.itemapi.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelUnitTest {

    @Test
    public void testCreateItem() {
        Item item1 = new Item();
        item1.setPrice(300);
        item1.setStatus("AVAIABLE");
        item1.setPhotoUrl("https://images.unsplash.com/photo-1497353865955-300799f7ef7a");
        item1.setName("batarag");
        item1.setDescription("throw your opponent without kill them");
        item1.setOwnerId(1);

        assertEquals("error initializing name attribute", "batarag", item1.getName());
        assertEquals(
                "error initializing photoUrl attribute",
                "https://images.unsplash.com/photo-1497353865955-300799f7ef7a",
                item1.getPhotoUrl()
        );
        assertEquals(
                "error initializing description attribute",
                "throw your opponent without kill them",
                item1.getDescription());
        assertEquals("error initializing status attribute", "AVAIABLE", item1.getStatus());
        assertEquals("error initializing ownerId attribute", 1, item1.getOwnerId());
        assertEquals("error initializing price attribute", 300, item1.getPrice());

        item1.setId(1000000000);
        item1.setName("Batpod");
        item1.setPhotoUrl("https://images.unsplash.com/photo-1530719071904-2073cd1806e5");
        item1.setDescription("To mobilize");
        item1.setStatus("AVAIABLE");
        item1.setOwnerId(1);
        item1.setPrice(5000);

        assertEquals(
                "Setter failed to operate",
                "" + item1.getId() + " | " + item1.getName() + " | " + item1.getPhotoUrl() + " | " +
                item1.getDescription() + " | " + item1.getStatus() + " | " + item1.getOwnerId(),
                item1.toString());
    }

    @Test
    public void testCreateTopBrandItem() {
        TopBrandItem item1 = new TopBrandItem();
        item1.setPrice(300);
        item1.setStatus("AVAIABLE");
        item1.setPhotoUrl("https://images.unsplash.com/photo-1497353865955-300799f7ef7a");
        item1.setName("batarag");
        item1.setDescription("throw your opponent without kill them");
        item1.setOwnerId(1);

        assertEquals("error initializing name attribute", "batarag", item1.getName());
        assertEquals(
                "error initializing photoUrl attribute",
                "https://images.unsplash.com/photo-1497353865955-300799f7ef7a",
                item1.getPhotoUrl()
        );
        assertEquals(
                "error initializing description attribute",
                "throw your opponent without kill them",
                item1.getDescription());
        assertEquals("error initializing status attribute", "AVAIABLE", item1.getStatus());
        assertEquals("error initializing ownerId attribute", 1, item1.getOwnerId());
        assertEquals("error initializing price attribute", 300, item1.getPrice());

        item1.setId(1000000000);
        item1.setName("Batpod");
        item1.setPhotoUrl("https://images.unsplash.com/photo-1530719071904-2073cd1806e5");
        item1.setDescription("To mobilize");
        item1.setStatus("AVAIABLE");
        item1.setOwnerId(1);
        item1.setPrice(5000);

        assertEquals(
                "Setter failed to operate",
                "" + item1.getId() + " | " + item1.getName() + " | " + item1.getPhotoUrl() + " | " +
                        item1.getDescription() + " | " + item1.getStatus() + " | " + item1.getOwnerId(),
                item1.toString());
    }

}

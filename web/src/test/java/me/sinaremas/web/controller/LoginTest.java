package me.sinaremas.web.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import me.sinaremas.web.controller.LoginView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LoginView.class)
public class LoginTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void logincek() throws Exception {
        String test = "";
        mockMvc.perform(get("/login").param("login", ""))
                .andExpect(content().string(containsString(test)));

    }

    // @Test
    // public void testUserLogin() throws Exception {
    //     RequestBuilder requestBuilder = post("/login")
    //             .param("username", testUser.getUsername())
    //             .param("password", testUser.getPassword());
    //             mockMvc.perform(requestBuilder)
    //             .andDo(print())
    //             .andExpect(status().isOk())
    //             .andExpect(cookie().exists("JSESSIONID"));
    // }
    
}


package me.sinaremas.web.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import me.sinaremas.web.controller.ContactView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ContactView.class)
public class ContactTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void resumeWithVisitor() throws Exception {
        String test = "";
        mockMvc.perform(get("/contact").param("contact", ""))
                .andExpect(content().string(containsString(test)));

    }

    
}


package me.sinaremas.web.util;


import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilUnitTest {
    @Test
    public void authenticationTest() {
        Authentication authentication = new Authentication();
        authentication.setUsername("FwP");
        authentication.setPassword("Gantengnya gak ada yang ngalahin");
        assertEquals("FwP", authentication.getUsername());
        assertEquals("Gantengnya gak ada yang ngalahin", authentication.getPassword());
    }
}

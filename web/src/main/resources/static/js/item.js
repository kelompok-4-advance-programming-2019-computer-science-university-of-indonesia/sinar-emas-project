
function parseItem() {
    $.ajax({
        type: 'GET',
        url: '/items',
        contentType: "application/json",
        success: function (data) {
            owl = $('#itemlist').owlCarousel({
                nav:true,
            });
            items = data._embedded.items;
            for (i = 0; i < items.length; i++) {
                item = items[i];
                tokenlink = item._links.self.href.split('/');
                itemId = tokenlink[tokenlink.length - 1];
                html_data = '                <div class="owl-item">\n' +
                    '                  <div class="product-grid">\n' +
                    '                    <div class="item">\n' +
                    '                      <div class="product-thumb">\n' +
                    '                        <div class="image product-imageblock"> <a  href="/checkout/?itemId=' + itemId +'"> <img data-name="product_image" src="' + item.photoUrl + '" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="' + item.photoUrl + '" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a>\n' +
                    '                          <div class="button-group text-center">\n' +
                    '                            <div class="wishlist"><a href="#"><span>wishlist</span></a></div>\n' +
                    '                            <div class="quickview"><a href="#"><span>Quick View</span></a></div>\n' +
                    '                            <div class="compare"><a href="#"><span>Compare</span></a></div>\n' +
                    '                            <div class="add-to-cart"><a href="#"><span>Add to cart</span></a></div>\n' +
                    '                          </div>\n' +
                    '                        </div>\n' +
                    '                        <div class="caption product-detail text-center">\n' +
                    '                          <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>\n' +
                    '                          <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">' + item.name + '</a></h6>\n' +
                    '                          <span class="price"><span class="amount"><span class="currencySymbol">$ </span>' + item.price + '</span>\n' +
                    '                          </span>\n' +
                    '                        </div>\n' +
                    '                      </div>\n' +
                    '                    </div>\n' +
                    '                  </div>' +
                    '                </div>';
                owl.trigger('add.owl.carousel', [jQuery(html_data)]);
                owl.trigger('refresh.owl.carousel');
            }

        }});
}


function getTopBrandItem() {
    $.ajax({
        type: 'GET',
        url: '/topBrandItems',
        contentType: "application/json",
        success: function (data) {
            owl = $('#topbrandlist').owlCarousel({
                nav:true,
            });
            items = data._embedded.topBrandItems;
            for (i = 0; i < items.length; i++) {
                item = items[i];
                tokenlink = item._links.self.href.split('/');
                itemId = tokenlink[tokenlink.length - 1];
                html_data = '                <div class="owl-item">\n' +
                    '                  <div class="product-grid">\n' +
                    '                    <div class="item">\n' +
                    '                      <div class="product-thumb">\n' +
                    '                        <div class="image product-imageblock"> <a  href="/checkout/?itemId=' + itemId +'"> <img data-name="product_image" src="' + item.photoUrl + '" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="' + item.photoUrl + '" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a>\n' +
                    '                          <div class="button-group text-center">\n' +
                    '                            <div class="wishlist"><a href="#"><span>wishlist</span></a></div>\n' +
                    '                            <div class="quickview"><a href="#"><span>Quick View</span></a></div>\n' +
                    '                            <div class="compare"><a href="#"><span>Compare</span></a></div>\n' +
                    '                            <div class="add-to-cart"><a href="#"><span>Add to cart</span></a></div>\n' +
                    '                          </div>\n' +
                    '                        </div>\n' +
                    '                        <div class="caption product-detail text-center">\n' +
                    '                          <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>\n' +
                    '                          <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">' + item.name + '</a></h6>\n' +
                    '                          <span class="price"><span class="amount"><span class="currencySymbol">$ </span>' + item.price + '</span>\n' +
                    '                          </span>\n' +
                    '                        </div>\n' +
                    '                      </div>\n' +
                    '                    </div>\n' +
                    '                  </div>' +
                    '                </div>';
                owl.trigger('add.owl.carousel', [jQuery(html_data)]);
                owl.trigger('refresh.owl.carousel');
            }
        }});
}
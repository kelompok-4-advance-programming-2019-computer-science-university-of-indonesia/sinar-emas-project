package me.sinaremas.web.controller;

import org.springframework.http.*;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public abstract class BaseView {
    public void getGreeting(RestTemplate restTemplate, Model model, String session) {
        if(session == null || session.equals("")) {
            model.addAttribute("usergreeting", "Hello Anonymous, silakan login!");
            model.addAttribute("user_link", "/login");
            model.addAttribute("user_account", "LOGIN");
        }
        else {
            HttpCookie cookie = ResponseCookie.from("session", session)
                    .path("/")
                    .build();

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", cookie.toString());
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map= new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);

            ResponseEntity<String> userEntity = restTemplate.exchange(
                    "http://userapi/authenticate/get_user_from_session",
                    HttpMethod.GET,
                    requestEntity,
                    String.class);

            String[] userData = userEntity.getBody().split("\n");
            model.addAttribute("usergreeting", "Hello " + userData[2] + ", selamat berbelanja");
            model.addAttribute("user_link", "#");
            model.addAttribute("user_account", userData[1]);
        }

    }
}

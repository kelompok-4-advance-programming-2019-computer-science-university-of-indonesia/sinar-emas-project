package me.sinaremas.web.controller;

import me.sinaremas.web.util.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;


@Controller
public class LoginView {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/login")
    public String view() {
        return "login";
    }

    @PostMapping(path="/login", consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String authenticate(HttpServletResponse response, @ModelAttribute Authentication authentication) {
        String username = authentication.getUsername();
        String password = authentication.getPassword();


        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", password);

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://userapi/authenticate/login",
                HttpMethod.POST,
                requestEntity,
                String.class);
        HttpHeaders responseHeaders = responseEntity.getHeaders();
        response.setHeader("Set-Cookie", responseHeaders.getFirst(HttpHeaders.SET_COOKIE));

        if (responseEntity.getBody().equals("LOGIN SUCCESS")) {
            return "redirect:";
        }
        else {
            return "login";
        }
    }
}
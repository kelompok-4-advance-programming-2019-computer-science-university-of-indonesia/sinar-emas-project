package me.sinaremas.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class CheckoutView extends BaseView {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/checkout")
    public String showCheckout(Model model, @CookieValue(value = "session", required = false) String session) {
        getGreeting(restTemplate, model, session);
        return "checkout_page";
    }
}
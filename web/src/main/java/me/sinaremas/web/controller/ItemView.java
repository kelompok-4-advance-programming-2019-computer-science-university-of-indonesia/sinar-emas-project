package me.sinaremas.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class ItemView {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/items")
    public ResponseEntity<String> getItem() {
        HttpHeaders requestHeaders = new HttpHeaders();

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);


        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://itemapi/items",
                HttpMethod.GET,
                requestEntity,
                String.class
        );

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .body(responseEntity.getBody());

    }

    @GetMapping("/items/{itemId}")
    public ResponseEntity<String> getItemFromId(@PathVariable(value="itemId") String itemId) {
        HttpHeaders requestHeaders = new HttpHeaders();

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);


        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://itemapi/items/" + itemId,
                HttpMethod.GET,
                requestEntity,
                String.class
        );

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .body(responseEntity.getBody());

    }

    @GetMapping("/topBrandItems")
    public ResponseEntity<String> getTopBrandItem() {
        HttpHeaders requestHeaders = new HttpHeaders();

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);


        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://itemapi/topBrandItems",
                HttpMethod.GET,
                requestEntity,
                String.class
        );

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .body(responseEntity.getBody());

    }

    @GetMapping("/topBrandItems/{itemId}")
    public ResponseEntity<String> getTopBrandItemFromId(@PathVariable(value="itemId") String itemId) {
        HttpHeaders requestHeaders = new HttpHeaders();

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);


        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://itemapi/topBrandItems/" + itemId,
                HttpMethod.GET,
                requestEntity,
                String.class
        );

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .body(responseEntity.getBody());

    }
}

package me.sinaremas.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;


@Controller
public class ContactView extends BaseView {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/contact")
    public String showContact(@RequestParam(name = "contact", required = false) String contact,
                     Model model, @CookieValue(value = "session", required = false) String session) {
        getGreeting(restTemplate, model, session);
        model.addAttribute("contact", contact);
        return "contact_us";
    }
}
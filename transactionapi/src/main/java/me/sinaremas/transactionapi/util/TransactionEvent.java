package me.sinaremas.transactionapi.util;

public class TransactionEvent {
    private int userId;
    private int itemId;

    public int getUserId() {
        return this.userId;
    }

    public int getItemId() {
        return this.itemId;
    }


    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

}

package me.sinaremas.transactionapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import me.sinaremas.transactionapi.util.TransactionEvent;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class TransactionController {

    @Autowired
    RestTemplate restTemplate;

    @PostMapping(path = "/buy_item", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> buyItem(@CookieValue("session") String session,
                                          @ModelAttribute TransactionEvent transactionEvent) {

        int userId = transactionEvent.getUserId();
        int itemId = transactionEvent.getItemId();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("userId", String.valueOf(userId));
        map.add("itemId", String.valueOf(itemId));


        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, requestHeaders);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://itemapi/update/on_buy",
                HttpMethod.POST,
                requestEntity,
                String.class
        );
        HttpCookie cookie = ResponseCookie.from("session", session)
                .path("/")
                .build();


        if (responseEntity.getBody().equals("UPDATE SUCCESS")) {
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body("TRANSACTION SUCCESS");
        } else {
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body("TRANSACTION FAILED");
        }
    }
}

package me.sinaremas.transactionapi.util;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionUtilUnitTest {
    @Test
    public void testTransactionEvent() {
        TransactionEvent transactionEvent = new TransactionEvent();
        transactionEvent.setItemId(12);
        transactionEvent.setUserId(34);
        assertEquals(12, transactionEvent.getItemId());
        assertEquals(34, transactionEvent.getUserId());
    }
}

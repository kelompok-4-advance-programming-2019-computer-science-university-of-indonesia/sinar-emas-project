package me.sinaremas.userapi.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

public class PasswordHashManagerTest {

    @Test
    public void testHashPassword() throws java.security.NoSuchAlgorithmException{
        final String password = "password";
        PasswordHashManager passwordHashManager = PasswordHashManager.getInstance();
        String hashedPassword = passwordHashManager.hashRawPassword(password);
        assertTrue(passwordHashManager.verifyPassword(password, hashedPassword));
    }

}
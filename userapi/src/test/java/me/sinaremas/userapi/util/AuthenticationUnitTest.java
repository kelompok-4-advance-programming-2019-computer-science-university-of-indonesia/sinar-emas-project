package me.sinaremas.userapi.util;

import java.lang.reflect.Field;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

public class AuthenticationUnitTest {

    @Test
    public void testSetter() throws NoSuchFieldException, IllegalAccessException {
        final Authentication auth = new Authentication();
        auth.setUsername("foo");
        auth.setPassword("faa");
        final Field fieldUsername = auth.getClass().getDeclaredField("username");
        final Field fieldPassword = auth.getClass().getDeclaredField("password");
        fieldUsername.setAccessible(true);
        fieldPassword.setAccessible(true);
        assertEquals("User field didn't match", fieldUsername.get(auth), "foo");
        assertEquals("Password field didn't match", fieldPassword.get(auth), "faa");
    }

    @Test
    public void testGetter() throws NoSuchFieldException, IllegalAccessException {
        final Authentication auth = new Authentication();
        final Field fieldUsername = auth.getClass().getDeclaredField("username");
        final Field fieldPassword = auth.getClass().getDeclaredField("password");
        fieldUsername.setAccessible(true);
        fieldPassword.setAccessible(true);
        fieldUsername.set(auth, "magic_username");
        fieldPassword.set(auth, "magic_password");
        final String resultUsername = auth.getUsername();
        final String resultPassword = auth.getPassword();
        assertEquals("field username wasn't retrieved properly", resultUsername, "magic_username");
        assertEquals("field password wasn't retrieved properly", resultPassword, "magic_password");
    }

}
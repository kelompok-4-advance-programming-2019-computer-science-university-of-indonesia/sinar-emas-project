package me.sinaremas.userapi.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SessionEncryptionManagerUnitTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void testEncryptText()throws java.security.NoSuchAlgorithmException,
            javax.crypto.IllegalBlockSizeException, java.security.InvalidKeyException,
            javax.crypto.NoSuchPaddingException, java.security.InvalidAlgorithmParameterException,
            javax.crypto.BadPaddingException, java.lang.Exception{
        String testString = "FwP Ganteng bgt!";
        SessionEncryptionManager sessionEncryptionManager = SessionEncryptionManager.getInstance();
        String encryptedTestString = sessionEncryptionManager.encrypt(testString);
        String decryptedTestString = sessionEncryptionManager.decrypt(encryptedTestString);
        assertEquals("Decrypted ciphertext must be same with plaintext", testString, decryptedTestString);
    }

    @Test(expected = InvalidSaltException.class)
    public void testInvalidSalt()  throws java.security.NoSuchAlgorithmException,
            javax.crypto.IllegalBlockSizeException, java.security.InvalidKeyException,
            javax.crypto.NoSuchPaddingException, java.security.InvalidAlgorithmParameterException,
            javax.crypto.BadPaddingException{
        String testString3 = "BwWhoBWBQu2/ibwUyaxS5/k1wc5PTPE3f+WYnToUGA66BoLDHOXMNc35tiZZeAvKVmX7WOwonXEpxOEhgF/voSRWjubNYRH5ovj2zkNENDA4ShOr47cWbeI16Jxv6/WRD7jYj84BRI66i2Gn16F9slbI50+udJeNeJj/hui/DpoTAG2pnfy33LoSKeOhb/1Ef/bfmb9Z07WJYP2zPJk6Mg==";
        SessionEncryptionManager sessionEncryptionManager = SessionEncryptionManager.getInstance();
        String decryptedTestString2 = sessionEncryptionManager.decrypt(testString3);
    }
}


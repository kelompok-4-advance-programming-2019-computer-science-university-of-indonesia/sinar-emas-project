package me.sinaremas.userapi.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import me.sinaremas.userapi.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaoUnitTest {
    @Autowired
    UserRepository userRepository;

    @Test
    @Transactional
    @Rollback(false)
    public void testAddUser() {
        User newTestUser = new User();
        newTestUser.setUsername("username");
        newTestUser.setPassword("B83EB8B53A821B040E05B3DABF248F52$9542112026E5820430CEFFC095A64770FE7C6FDFAB40014CB2A8AC722322E2D6");
        newTestUser.setPhotoUrl("https://thispersondoesnotexist.com/");
        newTestUser.setName("John");
        newTestUser.setBalance(1000);
        long initialCount = userRepository.getNumberOfUser();
        User savedUser = userRepository.save(newTestUser);
        System.out.println(savedUser.getId());
        long presentCount = userRepository.getNumberOfUser();
        assertEquals("Number of data after insertion must increase by one", presentCount, initialCount + 1);
        userRepository.deleteById(savedUser.getId());
    }

}

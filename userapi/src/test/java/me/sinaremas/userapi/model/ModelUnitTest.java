package me.sinaremas.userapi.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelUnitTest {

    @Test
    public void testAddUser() {
        User user1 = new User();
        user1.setUsername("username");
        user1.setPassword("passwordnya");
        user1.setPhotoUrl("https://thispersondoesnotexist.com/");
        user1.setName("John");
        user1.setBalance(1000);


        assertEquals("error initializing username attribute", "username", user1.getUsername());
        assertEquals("error initializing password attribute", "passwordnya", user1.getPassword());
        assertEquals(
                "error initializing photoUrl attribute",
                "https://thispersondoesnotexist.com/",
                user1.getPhotoUrl()
        );
        assertEquals("error initializing name attribute", "John", user1.getName());
        assertEquals("error initializing balance attribute", 1000, user1.getBalance());

        user1.setId(90909090);
        user1.setUsername("usernamekedua");
        user1.setPassword("passwordkeduanya");
        user1.setPhotoUrl("https://www.pexels.com/photo/adult-beard-boy-casual-220453/");
        user1.setName("Jeffrey");
        user1.setBalance(99999999);

        assertEquals(
                "Setter failed to operate",
                "" + user1.getId() + " | " + user1.getUsername() + " | " + user1.getName() + " | " + user1.getPhotoUrl()
                ,
                user1.toString()
        );
    }
}
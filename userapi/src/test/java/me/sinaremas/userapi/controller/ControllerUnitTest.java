package me.sinaremas.userapi.controller;

import me.sinaremas.userapi.dao.UserRepository;
import me.sinaremas.userapi.util.Authentication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllerUnitTest {

    @Autowired
    UserRepository userRepository;

    @Test
    @Transactional
    @Rollback(false)
    public void testLoginSuccess() throws java.security.NoSuchAlgorithmException,
            javax.crypto.IllegalBlockSizeException, java.security.InvalidKeyException,
            javax.crypto.NoSuchPaddingException, java.security.InvalidAlgorithmParameterException,
            javax.crypto.BadPaddingException{
        Authenticator authenticator = new Authenticator();
        Authentication authentication = new Authentication();
        authentication.setUsername("fwpidn");
        authentication.setPassword("akuganteng");
        authenticator.userRepository = this.userRepository;
        ResponseEntity<String> responseEntity = authenticator.login(authentication);
        String body = responseEntity.getBody();
        assertEquals("Login success", body, "LOGIN SUCCESS");
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testLoginFailed() throws java.security.NoSuchAlgorithmException,
            javax.crypto.IllegalBlockSizeException, java.security.InvalidKeyException,
            javax.crypto.NoSuchPaddingException, java.security.InvalidAlgorithmParameterException,
            javax.crypto.BadPaddingException{
        Authenticator authenticator = new Authenticator();
        Authentication authentication = new Authentication();
        authentication.setUsername("fwpidn");
        authentication.setPassword("akujelek");
        authenticator.userRepository = this.userRepository;
        ResponseEntity<String> responseEntity = authenticator.login(authentication);
        String body = responseEntity.getBody();
        assertEquals("Login success", body, "LOGIN FAILED");
    }

    @Test
    public void testDecodeSession() throws IllegalBlockSizeException,
            InvalidKeyException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, BadPaddingException,
            NoSuchPaddingException {
        Authenticator authenticator = new Authenticator();
        authenticator.userRepository = this.userRepository;
        String expectedResult = "33\n" +
                "fwpidn\n" +
                "Febriananda Wida Pramudita\n" +
                "http://www.gravatar.com/avatar/d76a75c9f59bb8efee47683dfa793775\n" +
                "10000";
        String session = "AwWhoBWBQu2/ibwUyaxS5/k1wc5PTPE3f+WYnToUGA66BoLDHOXMNc35tiZZeAvKVmX7WOwonXEpxOEhgF/voSRWjubNYRH5ovj2zkNENDA4ShOr47cWbeI16Jxv6/WRD7jYj84BRI66i2Gn16F9slbI50+udJeNeJj/hui/DpoTAG2pnfy33LoSKeOhb/1Ef/bfmb9Z07WJYP2zPJk6Mg==";

        String decodedSession = authenticator.getUserFromSession(session);

        assertEquals("Session not match", expectedResult, decodedSession);
    }
}
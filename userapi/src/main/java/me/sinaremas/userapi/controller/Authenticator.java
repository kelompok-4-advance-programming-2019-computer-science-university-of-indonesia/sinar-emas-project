package me.sinaremas.userapi.controller;

import java.time.Instant;
import java.security.NoSuchAlgorithmException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.BadPaddingException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import me.sinaremas.userapi.util.PasswordHashManager;
import me.sinaremas.userapi.util.Authentication;
import me.sinaremas.userapi.util.SessionEncryptionManager;
import me.sinaremas.userapi.dao.UserRepository;
import me.sinaremas.userapi.model.User;

@RestController()
@RequestMapping("/authenticate")
public class Authenticator {

    @Autowired
    UserRepository userRepository;

    @GetMapping(path = "/get_user_from_session")
    public String getUserFromSession(@CookieValue("session") String session) throws IllegalBlockSizeException,
            InvalidKeyException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, BadPaddingException,
            NoSuchPaddingException{
        SessionEncryptionManager sessionEncryptionManager = SessionEncryptionManager.getInstance();
        String[] sessionData = sessionEncryptionManager.decrypt(session).split("\n", 3);
        String username = sessionData[1];
        String password = sessionData[2];
        User user = userRepository.getUserFromUsernameAndPassword(username, password);
        return "" + user.getId() + "\n" + user.getUsername() + "\n" + user.getName() + "\n" + user.getPhotoUrl() + "\n" + user.getBalance();
    }

    @PostMapping(path="/login", consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> login(@ModelAttribute Authentication authentication)
            throws IllegalBlockSizeException, InvalidKeyException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, BadPaddingException, NoSuchPaddingException
    {
        String username = authentication.getUsername();
        String inputPassword = authentication.getPassword();
        String storedPassword = userRepository.getUserPassword(username);
        PasswordHashManager passwordHashManager = PasswordHashManager.getInstance();
        if(!passwordHashManager.verifyPassword(inputPassword, storedPassword)) {
            return ResponseEntity
                    .ok()
                    .body("LOGIN FAILED");
        }
        SessionEncryptionManager sessionEncryptionManager = SessionEncryptionManager.getInstance();
        String session = sessionEncryptionManager.encrypt(
                "" + Instant.now().getEpochSecond() + "\n" +
                authentication.getUsername() + "\n" +
                storedPassword
        );
        HttpCookie cookie = ResponseCookie.from("session", session)
                .path("/")
                .build();
        return ResponseEntity
                .ok()
                .header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body("LOGIN SUCCESS");
    }

}
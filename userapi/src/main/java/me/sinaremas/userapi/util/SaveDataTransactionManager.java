package me.sinaremas.userapi.util;

/*
 * Author: Febriananda Wida Pramudita
 * Ini template method gan
 */

import java.util.Random;

abstract class SaveDataTransactionManager {

    protected byte[] generateRandom16Bytes() {
        Random randomGenerator = new Random();
        byte[] block16Bytes = new byte[16];
        for(int ivIndex = 0; ivIndex < 16; ivIndex++) {
            block16Bytes[ivIndex] = (byte)randomGenerator.nextInt(256);
        }
        return block16Bytes;
    }

    protected byte[] concateByte(byte[] byteArray1, byte[] byteArray2) {
        byte[] result = new byte[byteArray1.length + byteArray2.length];
        System.arraycopy(byteArray1, 0, result, 0, byteArray1.length);
        System.arraycopy(byteArray2, 0, result, byteArray1.length, byteArray2.length);
        return result;
    }
}

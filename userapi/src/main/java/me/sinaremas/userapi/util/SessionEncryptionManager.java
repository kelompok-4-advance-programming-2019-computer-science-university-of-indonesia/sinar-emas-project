package me.sinaremas.userapi.util;

import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.BadPaddingException;
import javax.crypto.NoSuchPaddingException;

/*
 * This class is used to encrypt session
 * Created by: Febriananda Wida Pramudita
 * Refactored by:
 *      Febriananda Wida Pramudita
 *      Edrick Lainardi
 */


public class SessionEncryptionManager extends SaveDataTransactionManager{
    private final String base64Key = "EprVKKUUVlKoM8Q0na/qMw==";
    private final String salt = "the_session_salt";
    private static SessionEncryptionManager instance;

    public static SessionEncryptionManager getInstance() {
        if(instance == null) {
            instance = new SessionEncryptionManager();
        }
        return instance;
    }


    private byte[] getIv(byte[] ivCiphertext) {
        byte[] iv = new byte[16];
        System.arraycopy(ivCiphertext, 0, iv, 0, 16);
        return iv;
    }

    private byte[] getCiphertext(byte[] ivCiphertext) {
        byte[] ciphertext = new byte[ivCiphertext.length - 16];
        System.arraycopy(ivCiphertext, 16, ciphertext, 0, ivCiphertext.length - 16);
        return ciphertext;
    }

    public String encrypt(String value) throws NoSuchAlgorithmException, IllegalBlockSizeException,
            InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException, BadPaddingException{
        value = salt + value;
        byte[] key = Base64.getDecoder().decode(base64Key.getBytes());
        byte[] iv = generateRandom16Bytes();
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

        byte[] encrypted = cipher.doFinal(value.getBytes());
        return new String(Base64.getEncoder().encode(concateByte(iv, encrypted)));
    }

    public String decrypt(String base64IvCiphertext) throws IllegalBlockSizeException, InvalidKeyException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, BadPaddingException, NoSuchPaddingException {
        byte[] ivCiphertext = Base64.getDecoder().decode(base64IvCiphertext);
        byte[] iv = getIv(ivCiphertext);
        byte[] ciphertext = getCiphertext(ivCiphertext);
        byte[] key = Base64.getDecoder().decode(base64Key.getBytes());
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
        byte[] plaintextbytes = cipher.doFinal(ciphertext);
        String saltedPlaintext = new String(plaintextbytes);
        String saltedInput = saltedPlaintext.substring(0, 16);
        String plaintext = saltedPlaintext.substring(16, saltedPlaintext.length());
        if(!saltedInput.equals(salt)) {
            throw new InvalidSaltException("Acquired salt is not valid");
        }
        return plaintext;
    }
}


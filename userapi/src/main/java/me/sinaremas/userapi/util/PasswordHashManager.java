package me.sinaremas.userapi.util;


import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordHashManager extends SaveDataTransactionManager{

    private static PasswordHashManager instance;

    public static PasswordHashManager getInstance() {
        if(instance == null) {
            instance = new PasswordHashManager();
        }
        return instance;
    }

    private String hashPassword(byte[] salt, String rawPassword) throws NoSuchAlgorithmException {
        byte[] passwordByte = rawPassword.getBytes();
        byte[] saltedPassword = concateByte(salt, passwordByte);
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] hashedSaltPassword = sha256.digest(saltedPassword);
        return DatatypeConverter.printHexBinary(salt) + "$" + DatatypeConverter.printHexBinary(hashedSaltPassword);
    }

    public String hashRawPassword(String rawPassword) throws NoSuchAlgorithmException{
        byte[] salt = generateRandom16Bytes();
        return hashPassword(salt, rawPassword);
    }

    public boolean verifyPassword(String inputPassword, String hashedSaltedPasswordHex)
            throws NoSuchAlgorithmException {
        byte[] salt = new byte[16];
        for (int i = 0; i < 16; i++) {
            int index = i * 2;
            int j = Integer.parseInt(hashedSaltedPasswordHex.substring(index, index + 2), 16);
            salt[i] = (byte)j;
        }
        return hashedSaltedPasswordHex.equals(hashPassword(salt, inputPassword));
    }

}

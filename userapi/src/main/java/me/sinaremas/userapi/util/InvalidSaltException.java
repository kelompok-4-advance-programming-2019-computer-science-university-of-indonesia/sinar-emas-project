package me.sinaremas.userapi.util;

public class InvalidSaltException extends RuntimeException {
    public InvalidSaltException(String message) {
        super(message);
    }
}
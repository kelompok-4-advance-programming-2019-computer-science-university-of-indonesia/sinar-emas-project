package me.sinaremas.userapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import me.sinaremas.userapi.model.User;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    @Query("SELECT COUNT(*) FROM User")
    Long getNumberOfUser();

    @Query("SELECT u.password FROM User u WHERE u.username = :username")
    String getUserPassword(@Param("username") String username);

    @Query("SELECT u FROM User u WHERE u.username = :username AND u.password = :password")
    User getUserFromUsernameAndPassword(@Param("username")String username, @Param("password")String password);
}
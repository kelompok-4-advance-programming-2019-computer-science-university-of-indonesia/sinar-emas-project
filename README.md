[![coverage report](https://gitlab.com/kelompok-4-advance-programming-2019-computer-science-university-of-indonesia/sinar-emas-project/badges/master/coverage.svg)](https://gitlab.com/kelompok-4-advance-programming-2019-computer-science-university-of-indonesia/sinar-emas-project/commits/master)

[![pipeline status](https://gitlab.com/kelompok-4-advance-programming-2019-computer-science-university-of-indonesia/sinar-emas-project/badges/master/pipeline.svg)](https://gitlab.com/kelompok-4-advance-programming-2019-computer-science-university-of-indonesia/sinar-emas-project/commits/master)
---

untuk jalanin tiap server
- masuk ke folder project
- `mvn install`
- `java -jar target/*.jar`


tiap apps udah di set berada di port yang berbeda

---

## Developer Guide

### Installation
1. Install maven.
1. Install mysql.
   Untuk pengguna windows, mySQL bisa di download di https://dev.mysql.com/downloads/file/?id=484919
   Untuk pengguna linux dan macOS ketik `sudo apt install mysql` di terminal
1. Install php dan phpmyadmin (optional untuk menampilkan database dengan cantik).
1. Set password root menjadi `akuganteng`.
1. Setting java yang digunakan menjadi JDK dengan mengikut langkah berikut
   - klik kanan pada my computer dan tekan properties
   - buka advanced system settings 
   - tekan Environment Variables 
   - kemudian edit JAVA_HOME untuk menunjuk ke lokasi JDK softwarenya (Cth. C:\Program Files\Java\jdk1.6.0_02.)

### Checking Database
1. Nama database adalah `sinaremasdb`
1. Untuk melihat daftar tabel dapat mengetik ini pada mySQL shell
    ```
    SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='SINAREMASDB';
    ```
1. Untuk melihat daftar kolom pada suatu tabel. Misalkan tabel `ITEM`. Ketik berikut pada mySQL shell.
    ```
    SELECT TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='SINAREMASDB' AND TABLE_NAME='ITEM';
    ```
    
   atau anda dapat menggunakan phpmyadmin.


### Menjalankan test
1. Jalankan dahulu eureka service dengan mengetik `mvn install ; java -jar target/*.jar` di folder projek eurekaserver
1. Pergi ke projek yang akan dijalankan testnya, ketik `./test.sh`


### Notes
1. Penulis file ini sadar bahwa banyak sekali security hole pada projek ini. Security best practice telah diusahan oleh penulis file ini. Namun, dengan komposisi kemampuan kelompok yang ada, anggota lain merasa kesulitan mensetting sehingga dengan berat hati, saya membiarkan security hole tersebut demi mengejar deadline.
1. Repository ini awalnya ada private untuk mengurangi security hole karena komposisi kemampuan anggota kelompok. Namun requirement pada deskripsi tugas Advance Programming menyuruh agar repository dijadikan public.
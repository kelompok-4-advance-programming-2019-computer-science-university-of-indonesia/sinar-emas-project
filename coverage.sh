web_miss=$(cat web/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[1]}')
web_all=$(cat web/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[2]}' | \
    awk '{split($0,a,"<"); print a[1]}')
echo $web_miss $web_all

userapi_miss=$(cat userapi/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[1]}')
userapi_all=$(cat userapi/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[2]}' | \
    awk '{split($0,a,"<"); print a[1]}')
echo $userapi_miss $userapi_all

itemapi_miss=$(cat itemapi/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[1]}')
itemapi_all=$(cat itemapi/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[2]}' | \
    awk '{split($0,a,"<"); print a[1]}')
echo $itemapi_miss $itemapi_all

# topupapi_miss=$(cat topupapi/target/site/jacoco/index.html | \
#     awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
#     awk '{split($0,a," of "); print a[1]}')
# topupapi_all=$(cat topupapi/target/site/jacoco/index.html | \
#     awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
#     awk '{split($0,a," of "); print a[2]}' | \
#     awk '{split($0,a,"<"); print a[1]}')
# echo $topupapi_miss $topupapi_all

transactionapi_miss=$(cat transactionapi/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[1]}')
transactionapi_all=$(cat transactionapi/target/site/jacoco/index.html | \
    awk '{split($0,a,"Total</td><td class=\"bar\">"); print a[2]}' | \
    awk '{split($0,a," of "); print a[2]}' | \
    awk '{split($0,a,"<"); print a[1]}')
echo $transactionapi_miss $transactionapi_all

coverage=$(echo "scale=2;100 - 100*($web_miss + $userapi_miss + $itemapi_miss + $transactionapi_miss)/($web_all + $userapi_all + $itemapi_all + $transactionapi_all)" | bc)
echo "COVERAGE REPORT NGOHAHAHA : $coverage%"

